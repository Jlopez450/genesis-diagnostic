
		move.w writercursorpos,d0	;writing cursor before-hand fixed the no-cursor issue
		bsr calc_vram
		move.l d0,(a3)			;commented out because of possible graphical issue...
		move.w #$005E,(a4)		;...which was observed in the SEGA CD build
		
		lea (hex_write_screen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		
		move.l #$471A0003, (a3) ;reload digits
		move.w writerslot1,(a4)
		move.w writerslot2,(a4)
		move.w writerslot3,(a4)
		move.w writerslot4,(a4)
		move.w writerslot5,(a4)
		move.w writerslot6,(a4)
		move.w #':',(a4)
		move.w #'$',(a4)
		move.w dataslot1,(a4)
		move.w dataslot2,(a4)
		move.w dataslot3,(a4)
		move.w dataslot4,(a4)		
		bsr read_controller1
		bsr hexwriter_input_test
	    cmpi.b #$7f,d3       ;START button
		 beq write_data
	    cmpi.b #$ef,d3       ;B button
		 beq write_data_byte
	    cmpi.b #$8f,d3       ;ABC button
		 beq ramjump		 
		rte
ramjump:
		bsr unhold
		jmp $ff1000
		
hexwriter_input_test:
	    cmpi.b #$fb,d3       ;D pad left
		 beq writer_move_left
	    cmpi.b #$f7,d3       ;D pad righ
		 beq writer_move_right
	    cmpi.b #$fe,d3       ;D pad up
		 beq writer_move_up
	    cmpi.b #$fd,d3       ;D pad down
		 beq writer_move_down		 
		rts	
		
write_data:
        move.l #$00000000, d5
        move.l #$00000000, d2
		move.w writerslot1, d2
		eor.w #$b0,d2           ;convert from ASCII to HEX
		lsl.l #8, d2            ;shift to the first byte of our 24-bit address
		lsl.l #8, d2
		lsl.l #4, d2            ;20 shifts for the first digit.
        eor.l d2,d5             ;insert into the correct space to build our target address
        move.l #$00000000, d2		
		move.w writerslot2, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot3, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #4, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot4, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot5, d2
		eor.w #$b0,d2		
		lsl.l #4, d2
        eor.l d2,d5	
        move.l #$00000000, d2		
		move.w writerslot6, d2
		eor.w #$b0,d2		
        eor.l d2,d5
        move.l d5, a0          ;move the target address to an address register
		
        move.l #$00000000, d5
        move.l #$00000000, d2		
		move.w dataslot1, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #4, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w dataslot2, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w dataslot3, d2
		eor.w #$b0,d2		
		lsl.l #4, d2
        eor.l d2,d5	
        move.l #$00000000, d2		
		move.w dataslot4, d2
		eor.w #$b0,d2		
        eor.l d2,d5 ;D5 will have data, A0 will have address
		
		move.w d5,(a0)
		rte
		
write_data_byte:
        move.l #$00000000, d5
        move.l #$00000000, d2
		move.w writerslot1, d2
		eor.w #$b0,d2           ;convert from ASCII to HEX
		lsl.l #8, d2            ;shift to the first byte of our 24-bit address
		lsl.l #8, d2
		lsl.l #4, d2            ;20 shifts for the first digit.
        eor.l d2,d5             ;insert into the correct space to build our target address
        move.l #$00000000, d2		
		move.w writerslot2, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot3, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #4, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot4, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w writerslot5, d2
		eor.w #$b0,d2		
		lsl.l #4, d2
        eor.l d2,d5	
        move.l #$00000000, d2		
		move.w writerslot6, d2
		eor.w #$b0,d2		
        eor.l d2,d5
        move.l d5, a0          ;move the target address to an address register
		
        move.l #$00000000, d5
        move.l #$00000000, d2		
		move.w dataslot3, d2
		eor.w #$b0,d2		
		lsl.l #4, d2
        eor.l d2,d5	
        move.l #$00000000, d2		
		move.w dataslot4, d2
		eor.w #$b0,d2		
        eor.l d2,d5 ;D5 will have data, A0 will have address		
		move.b d5,(a0)
		rte		
writer_move_up:
        bsr unhold
        cmpi.w #$c79a,writercursorpos
		 beq writer_add_digit1
        cmpi.w #$c79c,writercursorpos
		 beq writer_add_digit2	
        cmpi.w #$c79e,writercursorpos
		 beq writer_add_digit3	
        cmpi.w #$c7a0,writercursorpos
		 beq writer_add_digit4	
        cmpi.w #$c7a2,writercursorpos
		 beq writer_add_digit5
        cmpi.w #$c7a4,writercursorpos
		 beq writer_add_digit6
		 
        cmpi.w #$c7aa,writercursorpos ;data
		 beq writer_add_digit7
        cmpi.w #$c7ac,writercursorpos ;data
		 beq writer_add_digit8
        cmpi.w #$c7ae,writercursorpos ;data
		 beq writer_add_digit9
        cmpi.w #$c7b0,writercursorpos ;data
		 beq writer_add_digit10		 
        rts
writer_add_digit1:
        cmpi.w #$00bf,writerslot1 ;don't go past $F
		 beq return        
		move.l #$471A0003, (a3)
		add.w #$01, writerslot1
		move.w writerslot1, (a4)
		rts
writer_add_digit2:
        cmpi.w #$00bf,writerslot2
		 beq return        
		move.l #$471c0003, (a3)
		add.w #$01, writerslot2
		move.w writerslot2, (a4)
		rts	
writer_add_digit3:
        cmpi.w #$00bf,writerslot3
		 beq return        
		move.l #$471e0003, (a3)
		add.w #$01, writerslot3
		move.w writerslot3, (a4)
		rts
writer_add_digit4:
        cmpi.w #$00bf,writerslot4
		 beq return        
		move.l #$47200003, (a3)
		add.w #$01, writerslot4
		move.w writerslot4, (a4)
		rts
writer_add_digit5:
        cmpi.w #$00bf,writerslot5
		 beq return        
		move.l #$47220003, (a3)
		add.w #$01, writerslot5
		move.w writerslot5, (a4)
		rts
writer_add_digit6:
        cmpi.w #$00bf,writerslot6
		 beq return        
		move.l #$47240003, (a3)
		add.w #$01, writerslot6
		move.w writerslot6, (a4)
		rts	
		
writer_add_digit7:
        cmpi.w #$00bf,dataslot1
		 beq return        
		move.l #$472a0003, (a3)
		add.w #$01, dataslot1
		move.w dataslot1, (a4)
		rts
writer_add_digit8:
        cmpi.w #$00bf,dataslot2
		 beq return        
		move.l #$472c0003, (a3)
		add.w #$01, dataslot2
		move.w dataslot2, (a4)
		rts	
writer_add_digit9:
        cmpi.w #$00bf,dataslot3
		 beq return        
		move.l #$472e0003, (a3)
		add.w #$01, dataslot3
		move.w dataslot3, (a4)
		rts	
writer_add_digit10:
        cmpi.w #$00bf,dataslot4
		 beq return        
		move.l #$47300003, (a3)
		add.w #$01, dataslot4
		move.w dataslot4, (a4)
		rts			
writer_move_down:
		bsr unhold
        cmpi.w #$c79a,writercursorpos
		 beq writer_sub_digit1
        cmpi.w #$c79c,writercursorpos
		 beq writer_sub_digit2
        cmpi.w #$c79e,writercursorpos
		 beq writer_sub_digit3	
        cmpi.w #$c7a0,writercursorpos
		 beq writer_sub_digit4	
        cmpi.w #$c7a2,writercursorpos
		 beq writer_sub_digit5	
        cmpi.w #$c7a4,writercursorpos
		 beq writer_sub_digit6	

        cmpi.w #$c7aa,writercursorpos ;data
		 beq writer_sub_digit7
        cmpi.w #$c7ac,writercursorpos ;data
		 beq writer_sub_digit8
        cmpi.w #$c7ae,writercursorpos ;data
		 beq writer_sub_digit9
        cmpi.w #$c7b0,writercursorpos ;data
		 beq writer_sub_digit10			 
        rts
writer_sub_digit1:
        cmpi.w #$00b0,writerslot1 ;don't undershoot below $0
		 beq return
        move.l #$471A0003, (a3)
		sub.w #$01, writerslot1
		move.w writerslot1, (a4)		
		rts
writer_sub_digit2:
        cmpi.w #$00b0,writerslot2
		 beq return
        move.l #$471c0003, (a3)
		sub.w #$01, writerslot2
		move.w writerslot2, (a4)		
		rts	
writer_sub_digit3:
        cmpi.w #$00b0,writerslot3
		 beq return
        move.l #$471e0003, (a3)
		sub.w #$01, writerslot3
		move.w writerslot3, (a4)		
		rts
writer_sub_digit4:
        cmpi.w #$00b0,writerslot4
		 beq return
        move.l #$47200003, (a3)
		sub.w #$01, writerslot4
		move.w writerslot4, (a4)		
		rts	
writer_sub_digit5:
        cmpi.w #$00b0,writerslot5
		 beq return
        move.l #$47220003, (a3)
		sub.w #$01, writerslot5
		move.w writerslot5, (a4)		
		rts	
writer_sub_digit6:
        cmpi.w #$00b0,writerslot6
		 beq return
        move.l #$47240003, (a3)
		sub.w #$01, writerslot6
		move.w writerslot6, (a4)		
		rts	

writer_sub_digit7:
        cmpi.w #$00b0,dataslot1
		 beq return        
		move.l #$472a0003, (a3)
		sub.w #$01, dataslot1
		move.w dataslot1, (a4)
		rts
writer_sub_digit8:
        cmpi.w #$00b0,dataslot2
		 beq return        
		move.l #$472c0003, (a3)
		sub.w #$01, dataslot2
		move.w dataslot2, (a4)
		rts	
writer_sub_digit9:
        cmpi.w #$00b0,dataslot3
		 beq return        
		move.l #$472e0003, (a3)
		sub.w #$01, dataslot3
		move.w dataslot3, (a4)
		rts	
writer_sub_digit10:
        cmpi.w #$00b0,dataslot4
		 beq return        
		move.l #$47300003, (a3)
		sub.w #$01, dataslot4
		move.w dataslot4, (a4)
		rts			
			
writer_move_left:
        cmpi.w #$c79a, writercursorpos
		 beq return
		clr d0
		swap d0
		move.w writercursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)			 
		sub.w #$02,writercursorpos
		clr d0
		swap d0
		move.w writercursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$005E,(a4)	
		bsr unhold		
		rts
		
writer_move_right:
        cmpi.w #$c7b0, writercursorpos
		 beq return
		clr d0
		swap d0
		move.w writercursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)			 
		add.w #$02,writercursorpos
		clr d0
		swap d0
		move.w writercursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$005E,(a4)
		bsr unhold
		rts	