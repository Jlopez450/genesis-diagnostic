		lea (image_screen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop	
		
	    bsr read_controller1
		bsr imagetestinput
		bsr draw_image_cursor
		rte
	
draw_image_cursor:
		clr d0
		swap d0
		move.w imagecursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$003c,(a4) ;<
		move.w #$002d,(a4) ;-
		move.w #$002d,(a4) ;-
		clr d0
		swap d0	
		move.w oldimagecursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		rts	

imagetestinput:
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq imagecursorup
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq imagecursordown
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7
		beq load_image	
		rts			
		
imagecursorup:
		cmpi.w #$c430,imagecursorpos
		beq return
		move.w imagecursorpos,oldimagecursorpos
		sub.w #$0080,imagecursorpos
		bsr unhold		
		rts		
imagecursordown:
		cmpi.w #$C830,imagecursorpos
		bge return
		move.w imagecursorpos,oldimagecursorpos		
		add.w #$0080,imagecursorpos
		bsr unhold			
		rts	
		
load_image:
		cmpi.w #$c430,imagecursorpos
		beq load_bleedtest
		cmpi.w #$c4B0,imagecursorpos
		beq load_sharptest
		cmpi.w #$C530,imagecursorpos
		beq load_overscantest
		cmpi.w #$C5B0,imagecursorpos
		beq load_stripetest
		cmpi.w #$C630,imagecursorpos
		beq load_checkertest	
		cmpi.w #$C6B0,imagecursorpos
		beq load_grad1	
		cmpi.w #$C730,imagecursorpos
		beq load_grad2	
		cmpi.w #$C7b0,imagecursorpos
		beq load_grad3
		cmpi.w #$C830,imagecursorpos
		beq load_dma
		rts
		
load_dma:
		move.w sr,sr_temp
		;move.w #$2700,sr
		lea (dmaimg)+70,a5
		move.w #$7c00,d4
		move.l #$FF0100,a0 
		bsr convert_image	
		move.w #$8F00,(a3)		;auto increment	of zero
		clr d0			
dmaloop:
		move.w #$8154,(a3)	;No V-INT, DMA enable, screen enable
		move.l	#$40000000, (A3)
vbwait1:
		btst.b	#3, $1(A3)
		beq	vbwait1
vbwait2:
		btst.b	#3, $1(A3)
		bne	vbwait2
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		move.w	D0, (a4)
		nop
		nop
		nop
		nop			
		move.l	#$93f7947f, (A3) ;DMA length ($47F7) (part)
		;move.l	#$934094AD, (A3) ;for whole transfer
		move.l	#$95009600, (A3) ;using long fixed the jitter issue for some reason...
		move.l	#$977f8114, (A3) 
		move.l	#$C0000080, (A3) ;Blast processing time!
		;bsr unhold
		bsr read_controller1
		cmpi.w #$0fff,d3
		bne nodma
		bra dmaloop
nodma:
		move.w #$8F02,(a3)		;auto increment	of 2
		move.w #$8174,(a3)
		;move.w sr_temp,sr
		bra dma_clear
convert_image:
        move.b -(a5),d5  ;first byte
        move.b -(a5),d0  ;second byte
		lsl.w #$08,d5	 ;position it!
		add.w d5,d0		 ;smash them together. (Can't use .W, because of odd numbered address)
		
		move.w d0,d1	 ;save a copy of the data in d1 and d2
		move.w d0,d2
		andi.w #$7C00,d0 ;get red channel
		andi.w #$03E0,d1 ;green
		andi.w #$001F,d2 ;blue
		lsr.w #$08,d0	 ;get correct value (11 shifts for red)
		lsr.w #$03,d0
		andi.w #$0E,d0	 ;Not needed? Extra bit seems to be ignored by VDP.
		lsr.w #$06,d1
		andi.w #$0E,d1   ;Keeping these enabled just to be safe.	
		lsr.w #$01,d2
		andi.w #$0E,d2		
		lsl.w #$08,d2	 ;shift color bits into correct places.
		lsl.w #$04,d1	
		eor.w d0,d2		 ;add red to finished product
		eor.w d1,d2		 ;add green
        move.w d2,(a0)+  ;write finished product in to memory.
        dbf d4,convert_image
        rts 
		
load_bleedtest:
		move.w #$8134,(a3)		
		lea (img_bleed),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_bleed),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_sharptest:
		move.w #$8134,(a3)		
		lea (img_sharpness),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_sharpness),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_overscantest:
		move.w #$8134,(a3)		
		lea (img_overscan),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_overscan),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_stripetest:
		move.w #$8134,(a3)		
		lea (img_stripes),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_stripes),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_checkertest:
		move.w #$8134,(a3)		
		lea (img_checker),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_checker),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_grad1:
		move.w #$8134,(a3)		
		lea (img_grad1),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_grad1),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_grad2:
		move.w #$8134,(a3)		
		lea (img_grad1),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_grad2),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
load_grad3:
		move.w #$8134,(a3)		
		lea (img_grad1),a5
		lea (decompress_loc),a1
		bsr decompress
		move.w #$9300,(a3) 		;DMA length
		move.w #$9446,(a3)
		move.w #$9500,(a3)      ;00
		move.w #$9688,(a3)      ;10
		move.w #$977f,(a3)      ;ff
		move.l #$70000080,(a3)  ;DMA VRAM 3000
		bsr generate_map
		move.w #$8238,(a3)
		move.w #$8407,(a3)
		lea (palette_grad3),a5
		move.w #$0f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8174,(a3)
		bra image_wait
		
image_wait:
		bsr unhold
image_waitloop:
		bsr read_controller1
		cmpi.w #$fff,d3
		beq image_waitloop
dma_clear:
		move.w #$8134,(a3)				
		lea (palette),a5
		move.w #$3f,d4
		move.l #$c0000000,(a3)	
		bsr vram_loop		
		move.w #$8230,(a3)
		move.w #$8405,(a3)		
		bsr unhold
		move.w #$8174,(a3)		
		rts

		