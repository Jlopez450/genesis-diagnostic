		cmpi.b #$ff,initskip
		beq memtestloop
		move.b #$ff,initskip
		move.w #0000,failures_68k
		move.w #0000,failures_VDP
		move.w #0000,failures_Z80	
		move.l #$60000002, (a3)	
		lea (ramscreen),a5
		bsr termtextloop
		
memtestloop:
		bsr read_controller_ramtest
		bsr draw_ram_cursor
		rte
		
ramtest:
		move.w #$0000,failures_68k
		lea (noise),a0
		lea ramteststart,a1
		move.w #$7FC0,d4
ramloop:	
		bsr update_screen
		move.w (a0)+,d0
		move.w d0,(a1)
		move.w (a1)+,d1
		cmp d0,d1
		bne failure
		dbf d4, ramloop
		bsr clear_testram
		rts
failure:
		add.w #$0001,failures_68k
		sub.l #$00000002,a1
		bsr update_screen
		add.l #$00000002,a1		
		bsr failloop
		dbf d4, ramloop
		bsr clear_testram		
		rts
		
clear_testram:
		lea ramteststart,a0
		move.w #$7FC0,d4
testram_loop:
		move.w #$0000,(a0)+
		dbf d4, testram_loop
		rts		
		
failloop:
		bsr read_controller1
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7
		beq return
		
		cmpi.b #$7f,d3
		bne failloop
		bsr unhold
		rts

update_screen:
		;eor.b #$ff,d2
		;tst d2
		;beq return

		move.l a1,d6
		sub.l #$00FF0000,d6
		move.l #$0000A39C,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l a1,d6
write68kfailures:		
		move.w failures_68K,d6
		move.l #$0000A414,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		rts		
		
Vramtest:
		move.w #$0000,failures_VDP
		lea (noise),a0
		move.w #$7FFF,d4
		move.l #$00000000,d6
Vramloop:	

		move.l d6,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w (a0),d1
		move.w (a0)+,(a4)
		
		add.l #$00000002,d6
		move.l d6,d0
		sub.l #$00000002,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d5
		cmp d1,d5
		bne Vfailure	
		dbf d4, Vramloop
		bra restart_VDP
Vfailure:
		add.w #$0001,failures_VDP
		dbf d4, Vramloop
		bra restart_VDP
		
restart_VDP:
        bsr setup_vdp
		bsr clear_vram		
		lea (font),a5
		move.l #$40000000,(a3)
		move.w #$0C00,d4
		bsr vram_loop		
		move.l #$0000A000,d0
		bsr calc_vram
		move.l d0, (a3)	
		lea (ramscreen),a5
		bsr termtextloop
		
		move.l #$58000000,(a3)
		lea (sprites),a5
		move.w #$0500,d4
		bsr vram_loop		
		
		move.l a1,d6
		move.w failures_VDP,d6
		move.l #$0000A614,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		bsr writez80failures
		bsr write68kfailures
		rts			

z80ramtest:
		move.w #$0000,failures_z80
		lea (noise),a0
		move.l #$00A00000,a1
		move.w #$1FFF,d4
 		move.w #$100,($A11100)
		move.w #$100,($A11200)  		
z80ramloop:	
		bsr update_screen_z80
		move.b (a0)+,d0
		move.b d0,(a1)
		move.b (a1)+,d1
		cmp d0,d1
		bne z80failure
		dbf d4, z80ramloop
		bsr clear_z80ram
		rts
z80failure:
		add.w #$0001,failures_z80
		sub.l #$00000001,a1
		bsr update_screen_z80	
		add.l #$00000001,a1		
		bsr failloop		
		dbf d4, z80ramloop
		rts		
		
update_screen_z80:
		;eor.b #$ff,d2
		;tst d2
		;beq return
		move.l a1,d6
		sub.l #$00FF0000,d6
		move.l #$0000A818,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l a1,d6
writez80failures:		
		move.w failures_z80,d6
		move.l #$0000A894,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		rts			

clear_z80ram:
		move.w #$1FFF,d4
 		move.w #$100,($A11100)
		move.w #$100,($A11200) 	
		move.l #$00A00000,a1		
z80clearloop:	
		move.b #$00,(a1)+
		dbf d4, z80clearloop
		rts		
		
calc_vram_read:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$0000,d0  ;attach vram read bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	

read_controller_ramtest:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq ramcursorup	
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq ramcursordown	
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7
		beq starttest		
		rts
		
starttest:
		cmpi.w #$c11c,ramcursorpos
		beq ramtest	
		cmpi.w #$C19C,ramcursorpos
		beq Vramtest			
		cmpi.w #$c21c,ramcursorpos
		beq z80ramtest	
		rts
	
ramcursorup:
		cmpi.w #$c11c,ramcursorpos
		beq return
		move.w ramcursorpos,oldramcursorpos
		sub.w #$0080,ramcursorpos
		bsr unhold		
		rts		
ramcursordown:
		cmpi.w #$c21c,ramcursorpos
		bge return
		move.w ramcursorpos,oldramcursorpos		
		add.w #$0080,ramcursorpos
		bsr unhold			
		rts			
		
draw_ram_cursor:
		clr d0
		swap d0
		move.w ramcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$003c,(a4) ;<
		move.w #$002d,(a4) ;-
		move.w #$002d,(a4) ;-
		clr d0
		swap d0	
		move.w oldramcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		rts			
		
 