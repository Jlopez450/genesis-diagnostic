 		lea (sound_screen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
	    bsr read_controller1
		bsr soundtestinput
		bsr draw_sound_cursor
		rte
soundtestinput:
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq soundcursorup
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq soundcursordown
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7
		beq pick_test	
		rts	
soundcursorup:
		cmpi.w #$c1aa,soundcursorpos
		beq return
		move.w soundcursorpos,oldsoundcursorpos
		sub.w #$0080,soundcursorpos
		bsr unhold		
		rts		
soundcursordown:
		cmpi.w #$c3aa,soundcursorpos
		bge return
		move.w soundcursorpos,oldsoundcursorpos		
		add.w #$0080,soundcursorpos
		bsr unhold			
		rts	
pick_test:
		cmpi.w #$c1aa,soundcursorpos	
		 beq test_fm
		cmpi.w #$c22a,soundcursorpos
		 beq test_psg		
		cmpi.w #$c2aa,soundcursorpos	
		 beq test_dac		
		cmpi.w #$c32a,soundcursorpos	
		 beq dac_left	
		cmpi.w #$c3aa,soundcursorpos	
		 beq dac_right		
		rts
draw_sound_cursor:
		clr d0
		swap d0
		move.w soundcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$003c,(a4) ;<
		move.w #$002d,(a4) ;-
		move.w #$002d,(a4) ;-
		clr d0
		swap d0	
		move.w oldsoundcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		rts			
		
test_FM:
        move.b #$2b,($A04000)         ;DAC enable register             
        move.b #$00,($A04001)         ;disable DAC     
		lea (testmusic+40),a2
		bsr music_driver
		bsr kill_ym2612
		rts
test_psg:
		lea (psg_track+40),a2
		bsr music_driver
		rts

dac_left:
		 move.b #$b6,($A04002)         ;pan           
        move.b #$80,($A04003)         ;left
		jmp dodac
dac_right:
		move.b #$b6,($A04002)         ;pan           
        move.b #$40,($A04003)         ;right
		jmp dodac
test_DAC:
		 move.b #$b6,($A04002)         ;pan           
        move.b #$c0,($A04003)         ;centre
dodac:		
        move.b #$2b,($A04000)         ;DAC enable register             
        move.b #$80,($A04001)         ;enable DAC             
        move.b #$2a,($A04000)         ;DAC register
		lea (DacDat),a2
		move.w #$0000,PCM_counter		
		bra play	
play:      
        bsr delay
		bsr test2612
		move.b (a2)+,$A04001      ;write to DAC	
		add.w #$0001,PCM_counter
		cmpi.w #$6110,PCM_counter
		 beq return
		bra play
delay:
        nop
        nop
        nop
        nop
        nop ;lots of NOPS for the low sample rate clip
        nop
        nop	
        nop		
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop	
        nop	
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop	
        nop			
        nop			
        nop			
        nop			
        nop			
        nop			
        nop			
        nop			
        nop			
        nop		
        nop					
		rts
test2612:
        move.b $A04001,d2
        btst #$80,d2
		beq delay 
		rts		