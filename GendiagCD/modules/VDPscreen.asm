		lea (VDP_screen,pc),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop	

		move.l #$00003000,d0   ;sprite write
		bsr calc_vram
		move.l d0,(a3)	
		move.w #$0120,(a4) ;knuckles y pos
		move.w #$0f01,(a4)
		move.w #$a0c0,(a4)
		move.w #$0088,(a4) ;knuckles x pos
		move.w #$0120,(a4) ;sonic y pos
		move.w #$0f02,(a4)
		move.w #$48d0,(a4)
		move.w #$00b0,(a4) ;sonic x pos	
		
		move.l #$0000c9bc,d0;tile map target
		bsr calc_vram
		move.l d0,(a3)
		lea (tilemap,pc),a5
		move.w #$0148,d4
		bsr vram_loop	
		rts
 
