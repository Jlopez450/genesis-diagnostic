		lea (screen1,pc),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		
		move.l #$0000c3aa,d0
		bsr calc_vram
		move.l d0,(a3)	
		move.l #$00000000,d2
		move.b  $A10001, d0
		
		clr d1
		move.w d0,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.w d0,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		bsr region_check
		bsr check_tmss
		bsr check_TAS
		bsr check_32X
		bsr displaybenchscore		
		rts
check_32X:
		move.l #$0000c696,d0
		bsr calc_vram
		move.l d0,(a3)
        cmpi.l #'MARS',$A130EC  ;32X
         beq has32x 
		 bra no32x
has32x:
		lea (yestext,pc),a5
		bsr termtextloop
		rts
no32x:	
		lea (notext,pc),a5
		bsr termtextloop
		rts	
check_tmss:		
		move.l #$0000c49c,d0
		bsr calc_vram
		move.l d0,(a3)		
		move.l #$00000000,d0
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq tmnono
		bra tmyesyes			
tmyesyes:
		lea (yestext,pc),a5
		bsr termtextloop
		rts
tmnono:
		lea (notext,pc),a5
		bsr termtextloop
		rts
check_TAS:
		move.l #$0000c5a6,d0
		bsr calc_vram
		move.l d0,(a3)	
		move.w #$0000,tas_test
		tas tas_test
		cmpi.b #$00, tas_test
		 beq notas
		 bra model3
		rts
notas:
		lea (nonetext,pc),a5
		bsr termtextloop
		rts
model3:
		lea (model3text,pc),a5
		bsr termtextloop
		rts
		
region_check:
		move.l #$0000c2a2,d0
		bsr calc_vram
		move.l d0,(a3)
		
		move.l #$00000000,d0
        move.b  $A10001, d0
		;andi.b #$f0,d0   ;strip TMSS bit
		;andi.b #$df,d0   ;strip expansion bit ($20)
		andi.b #$C0,d0   ;strip unneeded bits
		
		cmpi.b #$80, d0
		 beq NA 
		cmpi.b #$00, d0
		 beq Japan 		
		cmpi.b #$c0, d0
		 beq Europe 
		cmpi.b #$40, d0
		 beq Japan50
		bra unknown   ;if all else fails
		 
NA:
		lea (NAtext,pc),a5
		bsr termtextloop
		rts
Europe:	
		lea (Eurotext,pc),a5
		bsr termtextloop			
		rts	
Japan:
		lea (Japantext,pc),a5
		bsr termtextloop				
		rts	
Japan50:
		lea (Japan50text,pc),a5
		bsr termtextloop				
		rts	
unknown:
		lea (Unknowntext,pc),a5
		bsr termtextloop
		rts

displaybenchscore:
		move.l #$0000c7a2,d0
		bsr calc_vram
		move.l d0,(a3)		
		clr d0
		swap d0
			
		move.w benchmark_score,d7
		bsr hex2dec
		move.w d7,d0
			
        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)
        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$000f,d5		
		andi.w #$00ff, d5
    	add.w #$00b0,d5	
		move.w d5,(a4)
		rts			