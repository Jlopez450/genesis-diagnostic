		lea (CPU_screen,pc),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		move.l d0,D0_temp
		move.l d1,D1_temp
		move.l d2,D2_temp
		move.l d3,D3_temp
		move.l d4,D4_temp
		move.l d5,D5_temp
		move.l d6,D6_temp
		move.l d7,D7_temp
		move.l a0,A0_temp
		move.l a1,A1_temp
		move.l a2,A2_temp
		move.l a3,A3_temp
		move.l a4,A4_temp
		move.l a5,A5_temp
		move.l a6,A6_temp
		move.l a7,A7_temp
		move.w sr, sr_temp		
		move.w sr_temp,d0
		move.l #$4b320003,(a3);vram write $Cb32 ;all Es from now on are really Cs	
		bsr reg_dump_word

		move.l a0_temp,d0
		move.l #$488A0003,(a3);vram write $E88A
		bsr reg_dump				
		move.l a1_temp,d0
		move.l #$48A40003,(a3);vram write $E8a4
		bsr reg_dump						
		move.l a2_temp,d0
		move.l #$48BE0003,(a3);vram write $E8be
		bsr reg_dump				
		move.l a3_temp,d0
		move.l #$490a0003,(a3);vram write $E90a	
		bsr reg_dump	
		move.l a4_temp,d0
		move.l #$49240003,(a3);vram write $E924		
		bsr reg_dump	
		move.l a5_temp,d0
		move.l #$493E0003,(a3);vram write $E93E
		bsr reg_dump		
		move.l a6_temp,d0
		move.l #$498A0003,(a3);vram write $E98A
		bsr reg_dump	
		move.l a7_temp,d0
		move.l #$49A40003,(a3);vram write $E9a4
		bsr reg_dump

		move.l d0_temp,d0
		move.l #$458A0003,(a3);vram write $E58A
		bsr reg_dump
		move.l d1_temp,d0
		move.l #$45a40003,(a3);vram write $E5a4
		bsr reg_dump
		move.l d2_temp,d0
		move.l #$45BE0003,(a3);vram write $E5BE
		bsr reg_dump
		move.l d3_temp,d0
		move.l #$460A0003,(a3);vram write $E60a
		bsr reg_dump
		move.l d4_temp,d0
		move.l #$46240003,(a3);vram write $E624
		bsr reg_dump
		move.l d5_temp,d0
		move.l #$463E0003,(a3);vram write $E63D
		bsr reg_dump	
		move.l d6_temp,d0
		move.l #$468A0003,(a3);vram write $E68A
		bsr reg_dump
		move.l d7_temp,d0
		move.l #$46a40003,(a3);vram write $E6a4
		bsr reg_dump		
		rts