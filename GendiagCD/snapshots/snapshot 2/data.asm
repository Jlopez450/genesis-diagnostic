VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8230	;field A    
	dc.w $8300	;$833e	
	dc.w $8405	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3f		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000,$0000,$0000,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000

	dc.w $0000,$0206,$020C,$064E,$08AE,$0EEE,$0CAA,$0ECC
	dc.w $0866,$0222,$046A,$0008,$0080,$008E,$00AE,$0000 ;knuckles
	
	dc.w $0000,$0C42,$0E66,$0822,$08AE,$0CAA,$0EEE,$0222
	dc.w $046A,$0866,$0008,$000E,$0000,$0000,$0000,$0000 ;sonic

	dc.w $0000,$0E42,$0822,$0E66,$08AE,$046A,$0CAA,$0EEE
	dc.w $0080,$0866,$0646,$000E,$0008,$008E,$00AE,$0222 ;tornado
	
font:
	incbin "graphics/ascii.bin"
	incbin "graphics/hexascii.bin"
	
screen1:
	dc.b "                                        "
	dc.b " Gen Test V1.1 Copyright 2015 ComradeOj "
	dc.b "--------------System info---------------"	
	dc.b "                                        "
	dc.b "                                        "	
	dc.b " Detected region:                       "
	dc.b "                                        "	
	dc.b " Raw region register:                   "
	dc.b "                                        "	
	dc.b " TMSS present:                          "
	dc.b "                                        "
	dc.b " TAS functionality:                     "
	dc.b "                                        "
	dc.b " 32X found:                             "
	dc.b "                                        "
	dc.b " Benchmark score:                       "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b " If the controls move too quickly, try  "
	dc.b " starting the game with the MODE        "	
	dc.b " button on your controller held, or     "	
	dc.b " use a 3 button controller instead.     "	
	dc.b "                                        "
	dc.b "               Page 1/9                 "
	dc.b "                                       %"

exceptiontestscreen:
	dc.b "                                        "
	dc.b "----------Exception testing-------------"
	dc.b "                                        "
	dc.b "  WARNING: selecting any of these       "
	dc.b " options will trigger a 68k TRAP for    "
	dc.b " testing purposes, and require you to   "	
	dc.b " reset your console afterwards.         "
	dc.b "                                        "
	dc.b "          Trigger an address error      "
	dc.b "    Execute an illegal instruction      "
	dc.b "                    Divide by zero      "
	dc.b "         1010 instruction emulator      "
	dc.b "         1111 instruction emulator      "
	dc.b "       Cause a privilege violation      "
	dc.b " Cause a privilege violation (ALT)      "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  Use the D-pad to move the cursor,     "
	dc.b "  and press 'C' to choose an option.    "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
    dc.b "               Page 2/9                 "
	dc.b "                                       %"	
	
inputtestscreen:	
	dc.b "                                        "
	dc.b "--------------Input display-------------"
	dc.b "                                        "
	dc.b "   Controller port 1                    "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
    dc.b "                                        "
	dc.b "   Controller port 2                    "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
	dc.b "                                        "
	dc.b "   Extension port                       "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 3/9                 "
	dc.b "                                       %"		
	
hex_dump_screen:	
	dc.b "                                        "
	dc.b "------------Hex Data Dumper-------------"
	dc.b "                                        "
	dc.b "  Use the D-pad to enter an address.    "
	dc.b " Press START to accept your selection.  "
	dc.b " While viewing the data, press the 'C'  "
	dc.b " button to return to the main program.  "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               $000000                  "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  WARNING: Reading from oddly numbered  "
	dc.b " addresses will cause a crash on real   "
	dc.b " hardware, but may work in emulators.   "
	dc.b " (Locations ending in 1,3,5,7,9,B,D,F)  "	
	dc.b " Reading from certain memory locations  "
	dc.b " also may cause a crash.                "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 4/9                 "
	dc.b "                                       %"

hex_write_screen:	
	dc.b "                                        "
	dc.b "------------Hex Data Writer-------------"
	dc.b "                                        "
	dc.b "  Use the D-pad to enter an address,    "
	dc.b " and what data you want to write to     "
	dc.b " that address. Press START to write a   "
	dc.b " 16-bit word to the selected address,   "
	dc.b " or press B to write one byte (8 bits). "
	dc.b " Use the rightmost digits for your byte."
	dc.b "                                        "
	dc.b "       USE THIS TOOL WITH CAUTION!      "	
	dc.b "                                        "	
	dc.b "                                        "
	dc.b "            Address:Data                "
	dc.b "            $000000:$0000               "
	dc.b "                                        "
	dc.b "  Pressing A+B+C together will cause    "
	dc.b " the CPU to jump to $FF1000 where you   "
	dc.b " can enter program code beforehand, if  "
	dc.b " you so choose. To return to the main   "
	dc.b " program, use RTE (4E73) instead of RTS."
	dc.b "                                        "
	dc.b "  Like the hex dumping program, words   "
	dc.b " must be written to evenly numbered     "
	dc.b " addresses. However, bytes can be       "
	dc.b " written to even or odd addresses.      "
	dc.b "               Page 5/9                 "
	dc.b "                                       %"	
VDP_screen:	
	dc.b "                                        "
	dc.b "---------------VDP test-----------------"
	dc.b " The previous screen can be used to set "
	dc.b " the VDP's registers. Return to this    "
	dc.b " screen to view it's effects. The VDP's "
	dc.b " control port can be found at $C00004.  "
	dc.b " Examples:                              "
	dc.b " $8C81 - Disable shadow/highlight       "
	dc.b " $8C89 - Enable shadow/highlight        "
	dc.b " $8174 - Set V28 mode                   "
	dc.b " $817C - Set V30 mode (PAL only)*       "
	dc.b " $8C81 - Disable interlace mode         "
	dc.b " $8C87 - Enable interlace mode*         "
	dc.b " $8C80 - Set H32 mode*                  "
	dc.b " $8C81 - Set H40 mode                   "
	dc.b " $8174 - Set graphics mode 5            "
	dc.b " $8170 - Set graphics mode 4*           "
	dc.b " $8010 - Disable Palette Select         "
	dc.b " $8014 - Enable Palette Select          "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "          <-Sprites | Tiles->           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b " *May/will make the display unreadable  "
	dc.b "               Page 6/9                 "
	dc.b "                                        "	
	dc.b " This line is only visible in V30 mode! "
	dc.b "                                       %"	
	
CPU_screen:	
	dc.b "                                        "
	dc.b "------------M68K registers--------------"
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
	dc.b "           *Data registers*             "	
	dc.b "                                        "
	dc.b " D0:$???????? D1:$???????? D2:$???????? "
	dc.b " D3:$???????? D4:$???????? D5:$???????  "
	dc.b " D6:$???????? D7:$????????              "
	dc.b "                                        "
	dc.b "          *Address registers*           "	
	dc.b "                                        "
	dc.b " A0:$???????? A1:$???????? A2:$???????? "
	dc.b " A3:$???????? A4:$???????? A5:$???????? "
	dc.b " A6:$???????? A7:$????????              "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "        Status register:$????           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 8/9                 "
	dc.b "                                       %"		
	
meta_screen:	
	dc.b "                                        "
	dc.b "----------Meta info and credits---------"
	dc.b " Programming: ComradeOj                 "
	dc.b "     Testing: ComradeOj                 "
	dc.b "     FM test: ComradeOj                 "
	dc.b "    PSG test: Quartet - Unknown artist  "
	dc.b "    DAC test: SEGA                      "
	dc.b "                                        "
	dc.b " Special Thanks to:                     "	
	dc.b "                                        "	
	dc.b " Charles MacDonald                      "
	dc.b " For his VDP documentation.             "	
	dc.b "                                        "	
	dc.b " md.squee.co & wiki.megadrive.org       "
	dc.b " For their hardware documentation.      "
	dc.b "                                        "
	dc.b " Nemesis                                "
	dc.b " For his Exodus debug Genesis emulator. "
	dc.b "                                        "
	dc.b " Markus Persson                         "
	dc.b " For 0x10c                              "
	dc.b "                                        "
	dc.b " Programming language: 68K assembly     "
	dc.b " Lines of code: ~2300                   "
	dc.b " Build date: May 11, 2015               "	
	dc.b " ROM size: 84,044 bytes                 "	
	dc.b "               Page 9/9                 "
	dc.b "                                       %"	
	
		
Eurotext:
	dc.b "Europe         %"
NAtext:
	dc.b "North America  %"
Japantext:
	dc.b "Japan          %"
Japan50text:
	dc.b "Japan (50HZ)   %"	
yestext:
	dc.b "YES  %"
notext:
	dc.b "NO   %"	
nonetext:
	dc.b "None (normal)      %"
unknowntext:
	dc.b "Unknown        %"		
model3text:
	dc.b "Working (model 3)  %"
	
sprites:
	incbin "graphics/sprite1.bin"
	incbin "graphics/sprite2.bin"
	incbin "graphics/tiles.bin"
	
tilemap:
	incbin "graphics/tilemap.bin"	

	