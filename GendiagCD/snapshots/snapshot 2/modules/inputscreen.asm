		lea (inputtestscreen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		bsr read_controller1
		bsr read_controller2
		bsr read_controller3
		bsr display_input
		rte
		
display_input:
		move.l #$0000C21a,d0 ;port 1 raw TH0 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port1_th0,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port1_th0,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l #$0000C31a,d0 ;port 1 raw TH1 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port1_th1,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port1_th1,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)

		move.l #$0000C41a,d0 ;port 1 final display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port1_final,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port1_final,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)	

		move.l #$0000C59a,d0 ;port 2 raw TH0 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port2_th0,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port2_th0,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l #$0000C69a,d0 ;port 2 raw TH1 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port2_th1,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port2_th1,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)

		move.l #$0000C79a,d0 ;port 2 final display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port2_final,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port2_final,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l #$0000C91a,d0 ;port 3 raw TH0 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port3_th0,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port3_th0,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l #$0000Ca1a,d0 ;port 3 raw TH1 display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port3_th1,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port3_th1,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)

		move.l #$0000Cb1a,d0 ;port 3 final display
		bsr calc_vram
		move.l d0,(a3)	
		clr d1
		move.b port3_final,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		clr d1
		move.b port3_final,d1
		andi.w #$000F,d1
		eor.w #$00B0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)			
		rts
		
		