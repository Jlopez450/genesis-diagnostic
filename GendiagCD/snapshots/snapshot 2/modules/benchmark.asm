		move.b #$ff,d1
		add.b #$01,vblanks
		rte		

run_benchmark:
		clr d2
        move.b  $A10001, d2
		andi.b #$c0, d2
		cmpi.b #$c0, d2
		 beq loop_PAL 
		cmpi.b #$40, d2
		 beq loop_PAL 		 
		bra loop_NTSC
		
nobench:
	    move.w d6,d7
    	;bsr hex2dec		
		move.w d7, benchmark_score
	    move.b #$02,vblank_jump				
		bra loop
hang:		
		bra hang

loop_NTSC:
		cmpi.b #$ff,d1
		 bne loop_NTSC
		clr d7
		move.w #$0315,d7
		divs #$05,d7 
		mulu #$05,d7
		divu #$05,d7 
		muls #$04,d7
		divs #$04,d7 		
		lsl.w #$02,d7
		lsr.w #$01,d7
		ror.w #$01,d7
		bra forward_NTSC
		nop
forward_NTSC:
		move.w #$0020,d7
waitloop_NTSC:
		tas manian_devil
		add.l #$00000010,d2
		dbf d7,waitloop_NTSC
		asl.l #$00000002,d2
		sub.l #$10000543,d2
		ori.w #$ff00,d2
		andi.w #$00ff,d2
		eor.w #$0f0f,d2
		abcd d2,d2
		sbcd d2,d2		
		add.w #$0001,d6			;loop counter
		cmpi.b #60,vblanks
		 bge nobench		
		bra loop_NTSC
		
loop_PAL:
		cmpi.b #$ff,d1
		 bne loop_PAL
		clr d7
		move.w #$0315,d7
		divs #$05,d7 
		mulu #$05,d7
		divu #$05,d7 
		muls #$04,d7
		divs #$04,d7 		
		lsl.w #$02,d7
		lsr.w #$01,d7
		ror.w #$01,d7		
		bra forward_PAL
		nop
forward_PAL:
		move.w #$0020,d7
waitloop_PAL:
		tas manian_devil
		add.l #$00000010,d2
		dbf d7,waitloop_PAL
		asl.l #$00000002,d2
		sub.l #$10000543,d2
		ori.w #$ff00,d2
		andi.w #$00ff,d2
		eor.w #$0f0f,d2
		abcd d2,d2
		sbcd d2,d2		
		add.w #$0001,d6			;loop counter
		cmpi.b #50,vblanks
		 bge nobench		
		bra loop_PAL