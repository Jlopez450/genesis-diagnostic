		lea (exceptiontestscreen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		bsr read_controller1
		bsr exceptioninput
		bsr draw_cursor     ;because of this
		rte
exceptioninput:
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq exceptioncursorup
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq exceptioncursordown
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df,d7
		beq pick_exception		
		rts	
exceptioncursorup:
		cmpi.b #$01,exceptioncounter
		beq return
		sub.b #$01,exceptioncounter
		move.w cursorpos,oldcursorpos
		sub.w #$0080,cursorpos
		bsr unhold		
		rts		
exceptioncursordown:
		cmpi.b #$07,exceptioncounter
		bge return
		add.b #$01,exceptioncounter	
		move.w cursorpos,oldcursorpos		
		add.w #$0080,cursorpos
		bsr unhold			
		rts		
draw_cursor:
		clr d0
		swap d0
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$003c,(a4) ;<
		move.w #$002d,(a4) ;-
		move.w #$002d,(a4) ;-
		clr d0
		swap d0	
		move.w oldcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		;bsr unhold	;specifically this	
		rts	
pick_exception:
		lea (exceptions),a0
		clr d0
		swap d0
		clr d0
		move.b exceptioncounter,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

exceptions:		
 dc.w address_error		
 dc.w illegalinst		
 dc.w dividebyzero		
 dc.w line1010		
 dc.w line1111		
 dc.w privvio		
 dc.w privvio68010	
 
illegalinst:
		dc.w $00FF	
		rts
dividebyzero:
		clr d0					;set data register to zero
		divu d0,d0				;divide it by itself
		rts
line1111:
		dc.w $F000
		rts
line1010:
		dc.w $A000
		rts
address_error:
		move.w $FFFE01, d0	;read from a non word-aligned address
		rts
privvio:
		move.w #$0600,sr		;remove supervisor rights
		move.w #$2600,sr		;beg for them back and fail	
		rts
privvio68010:
		move.l #$00000000,d6
		move.w #$0600,sr		;remove supervisor rights
		move.w sr,d6			;read from status register
		rte