		cmpi.b #$ff, displaying
         beq check_hexreturn
		 
		move.w hexcursorpos,d0	;writing cursor before-hand fixed the no-cursor issue
		bsr calc_vram
		move.l d0,(a3)
		move.w #$005E,(a4)	
		
		lea (hex_dump_screen),a5
		move.l #$60000002,(a3) ;vram write $A000
		bsr termtextloop
		bsr read_controller1
		bsr hex_input_test
	    cmpi.b #$7f,d3       ;START button
		 beq accept		
		rte
check_hexreturn:
	    cmpi.b #$DF,d3       ;C
		 beq nohex
		rte 
nohex:
		;bsr read_controller1
		;bsr clear_layerA
		;bsr clear_layerB
		
        move.l #$40000010,(a3) ;write to vertical scroll RAM   
	    move.w #$0000,(a4)
		move.w #$0000,(a4)
		 
	    move.l #$0000fc00,d0
    	bsr calc_vram
		move.l d0,(a3)
		 
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		
        move.w #$000d,d2       ;number of rows			
		move.w #$00b0, slot1   ;pre-set ASCII character 0
		move.w #$00b0, slot2
		move.w #$00b0, slot3
		move.w #$00b0, slot4
		move.w #$00b0, slot5
		move.w #$00b0, slot6
		move.w #$c7a0, hexcursorpos		
		move.b #$00,displaying
		bra hexdumper
	
hex_input_test:
	    cmpi.b #$fb,d3       ;D pad left
		 beq move_left
	    cmpi.b #$f7,d3       ;D pad right
		 beq move_right
	    cmpi.b #$fe,d3       ;D pad up
		 beq move_up
	    cmpi.b #$fd,d3       ;D pad down
		 beq move_down	
		rts
accept:  ;take the individual number slots and crunches them into one target addresses
		move.b #$ff, displaying     ;set flag
        move.l #$00000000, d5
        move.l #$00000000, d2
		move.w slot1, d2
		eor.w #$b0,d2           ;convert from ASCII to HEX
		lsl.l #8, d2            ;shift to the first byte of our 24-bit address
		lsl.l #8, d2
		lsl.l #4, d2            ;20 shifts for the first digit.
        eor.l d2,d5             ;insert into the correct space to build our target address
        move.l #$00000000, d2		
		move.w slot2, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w slot3, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
		lsl.l #4, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w slot4, d2
		eor.w #$b0,d2		
		lsl.l #8, d2
        eor.l d2,d5
        move.l #$00000000, d2		
		move.w slot5, d2
		eor.w #$b0,d2		
		lsl.l #4, d2
        eor.l d2,d5	
        move.l #$00000000, d2		
		move.w slot6, d2
		eor.w #$b0,d2		
        eor.l d2,d5
        move.l d5, a0          ;move the target address to an address register
	    bsr clear_layerB       ;remove menu text and cursor		
        move.l #$00000000, d5	
        move.l #$00000000, d0
		
         move.l #$40000010,(a3) ;write to vertical scroll RAM   
		 move.w #$00FB,(a4)
		 move.w #$00FB,(a4)	   ;scroll down a little bit to center the display
		 
		 move.l #$0000fc00,d0
		 bsr calc_vram
		 move.l d0,(a3)
		 
		 move.w #$01fc,(a4)
		 move.w #$01fc,(a4)	   ;scroll left a bit to center the display
         move.w #$000d,d2       ;number of rows		
	   
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,d6
		move.l #$00000000,d0
		move.l #$00000000,d4
		bra display
		;rts
display:  ;convert hex data into ASCII characters to be displayed
		move.l d6,(a3)
		move.w #$0007,d4      ;row length
        bsr displayloop			
		move.w #$0000,(a4)    ;spacing		
		move.w #$0000,(a4)
        add.l  #$01000000, d6 ;skip a row	
        dbf d2,display
		rte
displayloop: 
        move.w (a0)+,d0
		move.w #$0000,(a4)    ;spacing
	
        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)

        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)

        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$00b0,d5
		move.w d5,(a4)

        move.l #$00000000, d5
	    move.w d0,d5
		andi.w #$000f,d5		
		andi.w #$00ff, d5
    	add.w #$00b0,d5	
		move.w d5,(a4)
        dbf d4,displayloop			
		rts		

cusorcontrol:
move_up:
        bsr unhold
        cmpi.w #$c7a0,hexcursorpos
		 beq add_digit1
        cmpi.w #$c7a2,hexcursorpos
		 beq add_digit2	
        cmpi.w #$c7a4,hexcursorpos
		 beq add_digit3	
        cmpi.w #$c7a6,hexcursorpos
		 beq add_digit4	
        cmpi.w #$c7a8,hexcursorpos
		 beq add_digit5
        cmpi.w #$c7aa,hexcursorpos
		 beq add_digit6			 
        rts
add_digit1:
        cmpi.w #$00bf,slot1 ;don't go past $F
		 beq return        
		move.l #$47200003, (a3)
		add.w #$01, slot1
		move.w slot1, (a4)
		rts
add_digit2:
        cmpi.w #$00bf,slot2
		 beq return        
		move.l #$47220003, (a3)
		add.w #$01, slot2
		move.w slot2, (a4)
		rts	
add_digit3:
        cmpi.w #$00bf,slot3
		 beq return        
		move.l #$47240003, (a3)
		add.w #$01, slot3
		move.w slot3, (a4)
		rts
add_digit4:
        cmpi.w #$00bf,slot4
		 beq return        
		move.l #$47260003, (a3)
		add.w #$01, slot4
		move.w slot4, (a4)
		rts
add_digit5:
        cmpi.w #$00bf,slot5
		 beq return        
		move.l #$47280003, (a3)
		add.w #$01, slot5
		move.w slot5, (a4)
		rts
add_digit6:
        cmpi.w #$00bf,slot6
		 beq return        
		move.l #$472a0003, (a3)
		add.w #$01, slot6
		move.w slot6, (a4)
		rts		
		
move_down:
		bsr unhold
        cmpi.w #$c7a0,hexcursorpos
		 beq sub_digit1
        cmpi.w #$c7a2,hexcursorpos
		 beq sub_digit2
        cmpi.w #$c7a4,hexcursorpos
		 beq sub_digit3	
        cmpi.w #$c7a6,hexcursorpos
		 beq sub_digit4	
        cmpi.w #$c7a8,hexcursorpos
		 beq sub_digit5	
        cmpi.w #$c7aa,hexcursorpos
		 beq sub_digit6		 
        rts
sub_digit1:
        cmpi.w #$00b0,slot1 ;don't undershoot below $0
		 beq return
        move.l #$47200003, (a3)
		sub.w #$01, slot1
		move.w slot1, (a4)		
		rts
sub_digit2:
        cmpi.w #$00b0,slot2
		 beq return
        move.l #$47220003, (a3)
		sub.w #$01, slot2
		move.w slot2, (a4)		
		rts	
sub_digit3:
        cmpi.w #$00b0,slot3
		 beq return
        move.l #$47240003, (a3)
		sub.w #$01, slot3
		move.w slot3, (a4)		
		rts
sub_digit4:
        cmpi.w #$00b0,slot4
		 beq return
        move.l #$47260003, (a3)
		sub.w #$01, slot4
		move.w slot4, (a4)		
		rts	
sub_digit5:
        cmpi.w #$00b0,slot5
		 beq return
        move.l #$47280003, (a3)
		sub.w #$01, slot5
		move.w slot5, (a4)		
		rts	
sub_digit6:
        cmpi.w #$00b0,slot6
		 beq return
        move.l #$472a0003, (a3)
		sub.w #$01, slot6
		move.w slot6, (a4)		
		rts			
			
move_left:
        cmpi.w #$c7a0, hexcursorpos
		 beq return
		clr d0
		swap d0
		move.w hexcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)			 
		sub.w #$02,hexcursorpos
		clr d0
		swap d0
		move.w hexcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$005E,(a4)	
		bsr unhold	
		rts
		
move_right:
        cmpi.w #$c7aa, hexcursorpos
		 beq return
		clr d0
		swap d0
		move.w hexcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)		;erase old cursor			 
		add.w #$02,hexcursorpos
		clr d0
		swap d0
		move.w hexcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$005E,(a4)
		bsr unhold
		rts	