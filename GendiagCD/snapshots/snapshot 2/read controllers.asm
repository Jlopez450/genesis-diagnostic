read_controller1:               ;d3 will have final controller reading!
		cmpi.b #$01, vblank_jump ;no input during benchmark
		 beq return
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
		move.b ($A10003), port1_th1			
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
		move.b ($A10003), port1_th0					
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3, port1_final	
		rts		
read_controller2:                
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A1000b) ;Set direction
	    move.b  #$40, ($A10005) ;TH = 1
		move.b ($A10005), port2_th1			
    	nop
	    nop
	    move.b  ($A10005), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10005)  ;TH = 0
		move.b ($A10005), port2_th0					
	    nop
	    nop
	    move.b	($A10005), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3, port2_final
		rts
read_controller3:               ;extension port
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A1000d) ;Set direction
	    move.b  #$40, ($A10007) ;TH = 1
		move.b ($A10007), port3_th1			
    	nop
	    nop
	    move.b  ($A10007), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10007)  ;TH = 0
		move.b ($A10007), port3_th0					
	    nop
	    nop
	    move.b	($A10007), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		move.b d3, port3_final
		rts	