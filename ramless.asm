ramless_ram_test:
		move.w #$2700, sr       ;disable ints
		
		lea ret2,a2
        bra Nsetup_vdp
ret2:		
		move.w #$8228,$4(a4)
		move.l #$C0000000,$4(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)
		
        move.l #$40000010,$4(a4)   ;write to vsram    
		move.w #$0000,(a4)
		move.w #$0000,(a4)			
		
		lea (font),a5
		move.l #$40000000,$4(a4)
		move.w #$0C00,d4		
		lea ret4,a2
		bra Nvram_loop
ret4:	
		
		lea ret30,a2
		lea (firstprompt),a5
		move.l #$60000002,$4(a4)
		bra Ntermtextloop	
ret30:	
		lea ret31,a2
		bra Nread_controller2
ret31:	
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7	
		beq saferamtest
		
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7
		beq load_normal
		
		bra ret30

saferamtest:		
		
		moveq #$00000000,d2
		;move.w #0000,failures_VDP ;vdp fail on high d2, 68k on low
		move.w #0000,a7	;failures z80
		move.l #$00000000,a6
		
		lea ret0,a6
		bra Nclear_regs
ret0:		
		
		lea ret6,a2
		bra Nclear_vram
ret6:			
		
		move.l #$60000002, $4(a4)		;vram A000
		lea (ramlessscreen),a5
		lea ret8,a2
		bra Ntermtextloop
ret8:				
					
Nloop:
		;lea ret10,a6
		bra Nread_controller		
;ret10:		
		;bra loop
		
		
Nramtest:
		move.w #$0000,d2
		lea (noise),a0
		move.l #$00FF0000,a1
		move.w #$7FFF,d4
Nramloop:	
		lea ret15,a2
		bra Nupdate_screen
ret15:
		move.w (a0)+,d0
		move.w d0,(a1)
		move.w (a1)+,d1
		cmp d0,d1
		bne Nfailure
		dbf d4, Nramloop
		lea ret1,a2
		bra Nclear_ram
ret1:		
		jmp nloop
Nfailure:
		add.w #$0001,d2
		sub.l #$00000002,a1
		lea ret16,a2
		bra Nupdate_screen
ret16:
		add.l #$00000002,a1		
		lea ret13,a6
		bra Nfailloop
ret13:		
		dbf d4, Nramloop
		jmp nloop
		
Nfailloop:
		lea ret11,a2
		bra Nread_controller2
ret11:
		
		cmpi.b #$7f,d3
		bne Nfailloop
ret14:		
		jmp (a6)
		
Nupdate_screen:
		;eor.b #$ff,d2
		;tst d2
		;beq return

		move.l a1,d6
		sub.l #$00FF0000,d6
		move.l #$639c0002,$4(a4)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l a1,d6
		
		move.w d2,d6
		move.l #$64140002,$4(a4)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		jmp (a2)		
		
NVramtest:
		move.l #$00000000,d2
		lea (noise),a0
		move.w #$7FFF,d4
		move.l #$00000000,d6
NVramloop:	

		move.l d6,d0
		lea ret17,a2
		bra Ncalc_vram
ret17:
		move.l d0,$4(a4)
		move.w (a0),d1
		move.w (a0)+,(a4)
		
		add.l #$00000002,d6
		move.l d6,d0
		sub.l #$00000002,d0
		lea ret18,a2
		bra Ncalc_vram_read
ret18:
		move.l d0,$4(a4)
		move.w (a4),d5
		cmp d1,d5
		bne NVfailure	
		dbf d4, NVramloop
		bra Nrestart_VDP
NVfailure:
		add.w #$0001,d2
		dbf d4, NVramloop
		bra Nrestart_VDP
		
Nrestart_VDP:
		lea ret3,a2
        bra Nsetup_vdp
ret3:		
		lea ret7,a2
		bra Nclear_vram	
ret7:		
		lea (font),a5
		move.l #$40000000,$4(a4)
		move.w #$0C00,d4
		
		lea ret5, a2
		bra Nvram_loop
ret5:		
		move.l #$60000002, $4(a4)	
		lea (ramlessscreen),a5
		lea ret9,a2
		bra Ntermtextloop
ret9:		
		
		move.l a1,d6
		move.w d2,d6
		move.l #$66140002,$4(a4)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)	
		swap d2
		jmp nloop			

Nz80ramtest:
		move.w #$0000,a7
		lea (noise),a0
		move.l #$00A00000,a1
		move.w #$1FFF,d4
 		move.w #$100,($A11100)
		move.w #$100,($A11200)  	
		moveq #$00000000,d0
		moveq #$00000000,d1
Nz80ramloop:	
		lea ret19,a2
		bra Nupdate_screen_z80
ret19:
		move.b (a0)+,d0
		move.b d0,(a1)
		move.b (a1)+,d1
		cmp d0,d1
		bne Nz80failure
		dbf d4, Nz80ramloop
		lea ret20,a2
		bra Nclear_z80ram
ret20:
		jmp nloop
Nz80failure:
		add.w #$0001,a7
		sub.l #$00000001,a1
		lea ret21,a2
		bra Nupdate_screen_z80
ret21:
		add.l #$00000001,a1	
		lea ret22,a6
		bra Nfailloop
ret22:		
		dbf d4, Nz80ramloop
		jmp nloop	
		
Nupdate_screen_z80:
		;eor.b #$ff,d2
		;tst d2
		;beq return
		move.l a1,d6
		sub.l #$00FF0000,d6
		move.l #$68180002,$4(a4)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		
		move.l a1,d6
		move.w a7,d6
		move.l #$68940002,$4(a4)
		
		move.w d6,d1
		andi.w #$F000,d1
		lsr.w #$08,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$0F00,d1
		lsr.w #$08,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		move.w d6,d1
		andi.w #$00F0,d1
		lsr.w #$04,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)
		move.w d6,d1
		andi.w #$000F,d1
		eor.w #$00b0,d1
		andi.w #$00ff,d1
		move.w d1,(a4)		
		jmp (a2)	
		
Ntermtextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
Ntextloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq Ntextret		
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,Ntextloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
Nemptyloop:		
		move.w #$0000,(a4)
		dbf d4,Nemptyloop
		bra Ntermtextloop
	
Ntextret:
		jmp (a2)
		
Nsetup_vdp:
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
NVDP_Loop:
        move.w (a5)+,$4(a4)       ;load setup array
	    nop
        dbf d4,NVDP_Loop
        jmp (a2)  
Nvram_Loop:
        move.w (a5)+,(a4)
        dbf d4,Nvram_Loop
        jmp (a2)
		
Nclear_regs:
		move.l #$00000000,d0
		move.l #$00000000,d1
		move.l #$00000000,d2
		move.l #$00000000,d3
		move.l #$00000000,d4
		move.l #$00000000,d5
		move.l #$00000000,d6
		move.l #$00000000,d7
		move.l #$00000000,a0
		move.l #$00000000,a1
		move.l #$00000000,a2
		move.l #$00000000,a4
		move.l #$00000000,a5
		move.l #$00000000,a7
		jmp (a6)
		
Nclear_ram:
		move.w #$7FFF,d0
		move.l #$FF0000,a0	
Nclear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, Nclear_ram_loop
		jmp (a2)
Nclear_vram:
		move.l #$40000000,$4(a4)
		move.w #$7fff,d4
Nvram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, Nvram_clear_loop
		jmp (a2)

Nclear_z80ram:
		move.w #$1FFF,d4
 		move.w #$100,($A11100)
		move.w #$100,($A11200) 	
		move.l #$00A00000,a1		
Nz80clearloop:	
		move.b #$00,(a1)+
		dbf d4, Nz80clearloop
		jmp (a2)
		
Ncalc_vram:
		;move.l d7,-(sp)
		move.l d0,d7
		andi.w #$C000,d7 ;get first two bits only
		lsr.w #$7,d7     ;shift 14 spaces to move it to the end
		lsr.w #$7,d7     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d7,d0      ;smash the two halves together	
		;move.l (sp)+,d7
		jmp (a2)	
		
Ncalc_vram_read:
		;move.l d7,-(sp)
		move.l d0,d7
		andi.w #$C000,d7 ;get first two bits only
		lsr.w #$7,d7     ;shift 14 spaces to move it to the end
		lsr.w #$7,d7     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$0000,d0  ;attach vram read bit
		swap d0          ;move d0 to high word
		eor.w d7,d0      ;smash the two halves together	
		;move.l (sp)+,d7
		jmp (a2)
		
Nread_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
	
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7
		beq Nramtest	
		move.b d3,d7
		or.b #$ef,d7		
		cmpi.b #$ef,d7
		beq NVramtest
		move.b d3,d7
		or.b #$Df,d7		
		cmpi.b #$df,d7
		beq Nz80ramtest			
		bra Nloop
Nread_controller2:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		jmp (a2)
