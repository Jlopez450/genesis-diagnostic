    include "ramdat.asm"
	include "header.asm"
	
start:                   
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss         ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		jmp ramless_ram_test
load_normal:		
		move.w #$2700, sr       ;disable interrupts	
		
        move.w  #$100,($A11100)	;send a Z80 a bus request
        move.w  #$100,($A11200)	;reset the Z80  
 		
		bsr clear_regs
        bsr setup_vdp
		bsr clear_vram
		bsr clear_slots 		;for data read/write sub-programs
		bsr clear_ram
		move.w #$00b0, slot1   ;pre-set ASCII character 0
		move.w #$00b0, slot2
		move.w #$00b0, slot3
		move.w #$00b0, slot4
		move.w #$00b0, slot5
		move.w #$00b0, slot6
		move.w #$00b0, writerslot1
		move.w #$00b0, writerslot2
		move.w #$00b0, writerslot3
		move.w #$00b0, writerslot4
		move.w #$00b0, writerslot5
		move.w #$00b0, writerslot6	
		move.w #$00b0, dataslot1
		move.w #$00b0, dataslot2
		move.w #$00b0, dataslot3
		move.w #$00b0, dataslot4	
		
		move.l #$40000000,(a3)  ;VRAM write $0000
		lea (font),a5
		move.w #$0BFF,d4		;data length
		bsr vram_loop
		
		move.l #$C0000000,(a3)  ;set CRAM write
		lea (palette),a5
		move.w #$003f,d4
		bsr vram_loop
		
		move.l #$58000000,(a3)
		lea (sprites),a5
		move.w #$0500,d4
		bsr vram_loop
		
        move.l #$40000010,(a3)  ;write to vertical scroll RAM   
	    move.w #$0000,(a4)
		move.w #$0000,(a4)
		
		move.b #$01,exceptioncounter
		move.b #$01,vblank_jump
		move.w #$c446,cursorpos
		move.w #$c1aa,soundcursorpos
		move.w #$c430,imagecursorpos
		move.w #$c11c,ramcursorpos
		move.w #$c7a0, hexcursorpos
		move.w #$c79a, writercursorpos	
		
        move.w #$2300, sr       ;enable ints
		bra run_benchmark
		
loop:
		bra loop		
		
HBlank:
        rte
		
check_page:
		move.b d3,d7
		;or.b #$f7,d7 ;OR conflicted with hex dumper
		cmpi.b #$B7,d7 ;right+A
		beq pageright
		move.b d3,d7
		;or.b #$fb,d7
		cmpi.b #$Bb,d7 ;left+A
		beq pageleft
		rts
		
pageright:
		move.b #$00,initskip
		cmpi.b #$0C, vblank_jump
		beq wraparound
		move.l #$70000003,(a3)	
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)	   ;clear sprites	
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		add.b #$01,vblank_jump
		bsr clear_slots		
		bsr clear_toplayer
		bsr unholdwithA	
		rts
		
pageleft:
		move.b #$00,initskip
		cmpi.b #$02,vblank_jump
		beq wraparound2
		move.l #$70000003,(a3)	
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		sub.b #$01,vblank_jump
		bsr clear_toplayer
		bsr clear_slots		
		bsr unholdwithA
		rts
wraparound:
		bsr clear_toplayer
		bsr clear_slots	
		move.b #$02,vblank_jump	
		bsr unholdwithA		
		rts	
wraparound2:
		bsr clear_toplayer
		bsr clear_slots	
		move.b #$0C,vblank_jump
		bsr unholdwithA		
		rts		

clear_toplayer:
		move.l #$0000c000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0700,d4
clear_toplayerloop:
		move.w #$0000,(a4)
		dbf d4,clear_toplayerloop
		rts
	
VBlank:
		; cmpi.b #$01, vblank_jump ;uncomment these 7 lines for 6 button compatibility
		 ; beq vbforward           ;skip delay if benchmarking
		; add.b #$01,VB_timer
		; cmpi.b #$0e,VB_timer     ;use lower numbers for more speed
		 ; bge vbforward           ;band-aid fix because of broken unhold routine on 6-button controllers
		; rte 
vbforward:
		;move.b #$00,VB_timer	   ;only needed when 6-button compatibility is enabled 
		bsr read_controller1
		bsr check_page
		lea (vblank_table),a0
		moveq #$00000000,d0
		move.b vblank_jump,d0
        lsl.b #$1,d0
		sub.w #$02,d0
		add.w d0,a0
		move.w (a0),d0
		move.l d0,a0
		jmp (a0)
		
vblank_table:
 dc.w benchmark
 dc.w firstscreen	 
 dc.w exceptionscreen
 dc.w inputscreen
 dc.w hexdumper
 dc.w hexwriter
 dc.w VDP
 dc.w soundtest
 dc.w CPUinfo
 dc.w memorytest
 dc.w testscreen
 dc.w credits 

CPUinfo:
		include "modules/cpuinfo.asm"
 
soundtest:
		include "modules/soundtest.asm"
		
VDP:
		include "modules/VDPscreen.asm"

credits:
		include "modules/creditscreen.asm"
 
hexwriter:
		include "modules/hexwriter.asm"
		
hexdumper: 
		include "modules/hexdumper.asm"		
		
inputscreen:
		include "modules/inputscreen.asm"

exceptionscreen: ;this is broken on all but fusion EDIT: FIXED IT
		include "modules/exceptiontest.asm"
		
benchmark:		
		include "modules/benchmark.asm"		
	
firstscreen:
		include "modules/infoscreen.asm"
		
memorytest:		
		include "modules/RAMtest.asm"
		
testscreen:		
		include "modules/testscreen.asm"
		
clear_layerB:	       
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.l d0,d6
        move.w  #$2fff, d4
clear_loopB:             
        move.w  #$0000,(a4)
	    nop
        dbf d4,clear_loopB
        rts	
clear_layerA:	       
		move.l #$0000c000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.l d0,d6
        move.w  #$2fff, d4
clear_loopA:             
        move.w  #$0000,(a4)
	    nop
        dbf d4,clear_loopA
        rts	

		
; termtextloop:					;for strings with terminators
		; move.b (a5)+,d4			;grab a character
		; cmpi.b #$25,d4			;% (end of text flag)
		 ; beq return
		; andi.w #$00ff,d4
        ; move.w d4,(a4)		
		; bra termtextloop
		
		
		
termtextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra termtextloop	
		
unhold:		
		bsr read_controller1
		cmpi.w #$fff,d3			;are buttons released?
		 beq return			 	;return if so
		bra unhold				;otherwise, keep waiting
		
unholdwithA:
		bsr read_controller1
		cmpi.w #$fBf,d3
		 beq return
		cmpi.w #$fff,d3
		 beq return		 
		bra unholdwithA	
		
		
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts
clear_regs:			
		move.l #$00000000,a0
		move.l #$00000000,a1		
		move.l #$00000000,a2
		move.l #$00000000,a3
		move.l #$00000000,a4		
		move.l #$00000000,a5
		move.l #$00000000,a6
		move.l #$00000000,d0
		move.l #$00000000,d1
		move.l #$00000000,d2
		move.l #$00000000,d3
		move.l #$00000000,d4
		move.l #$00000000,d5
		move.l #$00000000,d6
		move.l #$00000000,d7
		rts	
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach VRAM write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 

clear_slots:
		; move.w #$00b0, slot1   ;pre-set ASCII character 0
		; move.w #$00b0, slot2
		; move.w #$00b0, slot3
		; move.w #$00b0, slot4
		; move.w #$00b0, slot5
		; move.w #$00b0, slot6	
		
		; move.w #$00b0, writerslot1
		; move.w #$00b0, writerslot2
		; move.w #$00b0, writerslot3
		; move.w #$00b0, writerslot4
		; move.w #$00b0, writerslot5
		; move.w #$00b0, writerslot6	
		; move.w #$00b0, dataslot1
		; move.w #$00b0, dataslot2
		; move.w #$00b0, dataslot3
		; move.w #$00b0, dataslot4		
		rts		
hex2dec:              		;Roughly as efficient as cutting well-done steak with a spoon.	
		move.w d0, -(sp)
		move.w d1, -(sp)	;push registers
		move.w d3, -(sp)
		move.w #$0001,d0
		move.w #$0000,d1
		move.w d7,d3
		move.w #$0000,d7
		sub.w #$0001,d3		;Set BCD adder to 1
hex2decloop:
		cmpi.b #$99,d1		;are we overflowing?
		beq bcdoverflow	
		abcd d0,d1			;add binary coded decimal
bcd_ret:		
		dbf d3, hex2decloop	
		bra end_bcd
bcdoverflow:
		abcd d0,d7			;count number of times we hit 99
		move.w #$0000,d1	;clear first counter
		bra bcd_ret
end_bcd:
		lsl.w #$08,d7		;make some room for 8 bits
		eor.w d1,d7 		;combine overflow register with main register
		move.w (sp)+,d0		
		move.w (sp)+,d1		;pop registers
		move.w (sp)+,d3	
		rts		
		
clear_ram:
		lea ($ff0000),a0
		move.w #$07FF8,d4
ram_loop:
		move.w #$0000,(a0)+
		dbf d4, ram_loop
		rts
		
generate_map:                  ;generate a map for 320x200 images
        move.l #$180,d0         ;first tile
        move.w #$001c,d5
        move.l #$60000003,(a3) ;vram write to $E000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$00,(a4)
        dbf d4,maploop2
        dbf d5,superloop
        rts		
		
return:
		rts
returnint:
		rte	

	include "ramless.asm"
		
	include "decompress.asm"		
	include "read controllers.asm"		
	include "crashscreen.asm"		
	include "music_driver.asm"
	include "data.asm"

ROM_End:
              
              