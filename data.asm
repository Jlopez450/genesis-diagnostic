VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8230	;field A    
	dc.w $8300	;$833e	
	dc.w $8405	;field B	
	dc.w $8578	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3f		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000,$0000,$0000,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000

	dc.w $0000,$0206,$020C,$064E,$08AE,$0EEE,$0CAA,$0ECC
	dc.w $0866,$0222,$046A,$0008,$0080,$008E,$00AE,$0000 ;knuckles
	
	dc.w $0000,$0C42,$0E66,$0822,$08AE,$0CAA,$0EEE,$0222
	dc.w $046A,$0866,$0008,$000E,$0000,$0000,$0000,$0000 ;sonic

	dc.w $0000,$0E42,$0822,$0E66,$08AE,$046A,$0CAA,$0EEE
	dc.w $0080,$0866,$0646,$000E,$0008,$008E,$00AE,$0222 ;tornado
	
font:
	incbin "graphics/ascii.bin"
	incbin "graphics/hexascii.bin"
	
screen1:
	dc.b "                                        "
	dc.b " Gen Test V3.1 Copyright 2022 ComradeOj "
	dc.b "--------------System info---------------"	
	dc.b "                                        "
	dc.b "                                        "	
	dc.b " Detected region:                       "
	dc.b "                                        "	
	dc.b " Raw region register:                   "
	dc.b "                                        "	
	dc.b " TMSS present:                          "
	dc.b "                                        "
	dc.b " TAS functionality:                     "
	dc.b "                                        "
	dc.b " 32X found:                             "
	dc.b "                                        "
	dc.b " Benchmark score:                       "
	dc.b "                                        "
	dc.b " CPU Type:                              "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b " Press A+left or right to switch pages. "	
	dc.b "                                        "
	dc.b "               Page 1/11                "
	dc.b "                                       %"

exceptiontestscreen:
	dc.b "                                        "
	dc.b "----------Exception testing-------------"
	dc.b "                                        "
	dc.b "  WARNING: selecting any of these       "
	dc.b " options will trigger a 68k TRAP for    "
	dc.b " testing purposes, and require you to   "	
	dc.b " reset your console afterwards.         "
	dc.b "                                        "
	dc.b "          Trigger an address error      "
	dc.b "    Execute an illegal instruction      "
	dc.b "                    Divide by zero      "
	dc.b "         1010 instruction emulator      "
	dc.b "         1111 instruction emulator      "
	dc.b "       Cause a privilege violation      "
	dc.b " Cause a privilege violation (ALT)      "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  Use the D-pad to move the cursor,     "
	dc.b "  and press 'C' to choose an option.    "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
    dc.b "               Page 2/11                "
	dc.b "                                       %"	
	
inputtestscreen:	
	dc.b "                                        "
	dc.b "--------------Input display-------------"
	dc.b "                                        "
	dc.b "   Controller port 1                    "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
    dc.b "                                        "
	dc.b "   Controller port 2                    "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
	dc.b "                                        "
	dc.b "   Extension port                       "
	dc.b "     Raw TH0:                           "
	dc.b "                                        "	
	dc.b "     Raw TH1:                           "
	dc.b "                                        "	
	dc.b "       Final:                           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 3/11                "
	dc.b "                                       %"		
	
hex_dump_screen:	
	dc.b "                                        "
	dc.b "------------Hex Data Dumper-------------"
	dc.b "                                        "
	dc.b "  Use the D-pad to enter an address.    "
	dc.b " Press START to accept your selection.  "
	dc.b " While viewing the data, press any      "
	dc.b " button to return to the main program.  "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               $000000                  "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  WARNING: Reading from oddly numbered  "
	dc.b " addresses will cause a crash on real   "
	dc.b " hardware, but may work in emulators.   "
	dc.b " (Locations ending in 1,3,5,7,9,B,D,F)  "	
	dc.b " Reading from certain memory locations  "
	dc.b " also may cause a crash.                "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 4/11                "
	dc.b "                                       %"

hex_write_screen:	
	dc.b "                                        "
	dc.b "------------Hex Data Writer-------------"
	dc.b "                                        "
	dc.b "  Use the D-pad to enter an address,    "
	dc.b " and what data you want to write to     "
	dc.b " that address. Press START to write a   "
	dc.b " 16-bit word to the selected address,   "
	dc.b " or press B to write one byte (8 bits). "
	dc.b " Use the rightmost digits for your byte."
	dc.b "                                        "
	dc.b "       USE THIS TOOL WITH CAUTION!      "	
	dc.b "                                        "	
	dc.b "                                        "
	dc.b "            Address:Data                "
	dc.b "            $000000:$0000               "
	dc.b "                                        "
	dc.b "  Pressing A+B+C together will cause    "
	dc.b " the CPU to jump to $FF1000 where you   "
	dc.b " can enter program code beforehand, if  "
	dc.b " you so choose. To return to the main   "
	dc.b " program, use RTE (4E73) instead of RTS."
	dc.b "                                        "
	dc.b "  Like the hex dumping program, words   "
	dc.b " must be written to evenly numbered     "
	dc.b " addresses. However, bytes can be       "
	dc.b " written to even or odd addresses.      "
	dc.b "               Page 5/11                "
	dc.b "                                       %"	
VDP_screen:	
	dc.b "                                        "
	dc.b "---------------VDP test-----------------"
	dc.b " The previous screen can be used to set "
	dc.b " the VDP's registers. Return to this    "
	dc.b " screen to view it's effects. The VDP's "
	dc.b " control port can be found at $C00004.  "
	dc.b " Examples:                              "
	dc.b " $8C81 - Disable shadow/highlight       "
	dc.b " $8C89 - Enable shadow/highlight        "
	dc.b " $8174 - Set V28 mode                   "
	dc.b " $817C - Set V30 mode (PAL only)*       "
	dc.b " $8C81 - Disable interlace mode         "
	dc.b " $8C87 - Enable interlace mode*         "
	dc.b " $8C00 - Set H32 mode*                  "
	dc.b " $8C81 - Set H40 mode                   "
	dc.b " $8174 - Set graphics mode 5            "
	dc.b " $8170 - Set graphics mode 4*           "
	dc.b " $8010 - Disable Palette Select         "
	dc.b " $8014 - Enable Palette Select          "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "          <-Sprites | Tiles->           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b " *May/will make the display unreadable  "
	dc.b "               Page 6/11                "
	dc.b "                                        "	
	dc.b " This line is only visible in V30 mode! "
	dc.b " An Easter egg, or PAL only feature?   %"	
sound_screen:	
	dc.b "                                        "
	dc.b "--------------Sound test----------------"
	dc.b "                                        "
	dc.b "    Test FM                             "
	dc.b "    Test PSG                            "
	dc.b "    Test DAC (centre)                   "
	dc.b "    Test DAC (left)                     "
	dc.b "    Test DAC (right)                    "
	dc.b "                                        "
	dc.b "    Press 'C' to select an option       "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  Like the VDP, the Genesis sound chips "
	dc.b " can be written to using the included   "
	dc.b " 'Hex Data Writer' program. The sound   "
	dc.b " chip's addresses are as follows:       "
	dc.b "                                        "
	dc.b "       SN76489 PSG - $C00011            "
	dc.b "  YM2612 Address 0 - $A04000            "
	dc.b "  YM2612 Address 1 - $A04001            "
	dc.b "     YM2612 Data 0 - $A04002            "
	dc.b "     YM2612 Data 1 - $A04003            "
	dc.b "                                        "
	dc.b "  In all cases, data must be written in "
	dc.b "  bytes, not words.                     "
	dc.b "                                        "	
	dc.b "               Page 7/11                "
	dc.b "                                       %"	
	
CPU_screen:	
	dc.b "                                        "
	dc.b "------------M68K registers--------------"
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
    dc.b "                                        "
	dc.b "           *Data registers*             "	
	dc.b "                                        "
	dc.b " D0:$???????? D1:$???????? D2:$???????? "
	dc.b " D3:$???????? D4:$???????? D5:$???????  "
	dc.b " D6:$???????? D7:$????????              "
	dc.b "                                        "
	dc.b "          *Address registers*           "	
	dc.b "                                        "
	dc.b " A0:$???????? A1:$???????? A2:$???????? "
	dc.b " A3:$???????? A4:$???????? A5:$???????? "
	dc.b " A6:$???????? A7:$????????              "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "        Status register:$????           "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 8/11                "
	dc.b "                                       %"		
	
meta_screen:	
	dc.b "                                        "
	dc.b "----------Meta info and credits---------"
	dc.b " Programming: ComradeOj                 "
	dc.b "     Testing: ComradeOj                 "
	dc.b "     FM test: ComradeOj                 "
	dc.b "    PSG test: ComradeOj                 "
	dc.b "    DAC test: SEGA                      "
	dc.b "                                        "
	dc.b " Special Thanks to:                     "	
	dc.b "                                        "	
	dc.b " Charles MacDonald                      "
	dc.b " For his VDP documentation.             "	
	dc.b "                                        "	
	dc.b " md.squee.co & wiki.megadrive.org       "
	dc.b " For their hardware documentation.      "
	dc.b "                                        "
	dc.b " Nemesis                                "
	dc.b " For his Exodus debug Genesis emulator. "
	dc.b "                                        "
	dc.b " Markus Persson                         "
	dc.b " For 0x10c                              "
	dc.b "                                        "
	dc.b " Programming language: 68K assembly     "
	dc.b " Lines of code: ~2800                   "
	dc.b " Build date: Aug 24, 2022               "	
	dc.b " ROM size: 226,202 bytes                "	
	dc.b "               Page 11/11               "
	dc.b "                                       %"	
	
image_screen:	
	dc.b "                                        "
	dc.b "---------------Test images--------------"
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "      Press 'C' to select an image      "
	dc.b " When viewing, hit any button to return "
	dc.b "                                        "
	dc.b "       Color bleed test                 "
	dc.b "         Sharpness test                 "	
	dc.b "          Overscan test                 "
	dc.b "           Stripes test                 "
	dc.b "           Checker test                 "
	dc.b "      Red/Blue gradient                 "
	dc.b "   Green/White gradient                 "
	dc.b " Violet/Yellow gradient                 "
	dc.b "       Direct Color DMA                 "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "               Page 10/11               "
	dc.b "                                        "	
	dc.b "                                       %"	

ramscreen:	
	dc.b "                                        "
	dc.b "----------------RAM test----------------"
	dc.b " Test 68k RAM                           "
	dc.b "    Test VRAM                           "
	dc.b " Test Z80 RAM                           "
	dc.b "           *68K RAM results*            "
	dc.b "                                        "	
	dc.b " Progress: $FFXXXX/$FFFFFF              "
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b "           *VDP RAM results*            "
	dc.b "                                        "	
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b "           *Z80 RAM results*            "
	dc.b "                                        "	
	dc.b " Progress: $XXXX/$1FFF                  "
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b " Press 'C' to select an option.         "	
	dc.b "                                        "
	dc.b " Warning! Testing VRAM will cause the   "
	dc.b " Screen to temporarily glitch up.       "	
	dc.b " The program pauses when it finds an    "
	dc.b " error. Press START to resume. Hold     "
	dc.b " 'B' while testing to skip the pause.   "
	dc.b "               Page 9/11                "
	dc.b "                                       %"	
	
firstprompt:
	dc.b "                                        "			
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "	
	dc.b " Press A to load the program normally.  "
	dc.b " Press B to load memory diagnostic.     "		
	dc.b "                                        "		
	dc.b " Note: The main program contains a      "		
	dc.b " memory diagnostic, but a console with  "		
	dc.b " damaged memory may have trouble        "		
	dc.b " loading the main program.              "		
	dc.b "                                        "		
	dc.b " If you suspect your console has memory "		
	dc.b " issues, it is recommended that you     "		
	dc.b " press B.                               "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                        "		
	dc.b "                                       %"	

ramlessscreen:
	dc.b "                                        "
	dc.b "     Genesis RAM test by ComradeOj      "
	dc.b " Press 'A' to test 68K, press 'B' to    "
	dc.b " test VRAM, and press 'C' to test Z80.  "
	dc.b "                                        "
	dc.b "           *68K RAM results*            "
	dc.b "                                        "	
	dc.b " Progress: $FFXXXX/$FFFFFF              "
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b "             *VRAM results*             "
	dc.b "                                        "	
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b "           *Z80 RAM results*            "
	dc.b "                                        "	
	dc.b " Progress: $XXXX/$1FFF                  "
	dc.b " Failures:                              "
	dc.b "                                        "
	dc.b " Warning! Testing VRAM will cause the   "
	dc.b " Screen to temporarily glitch up.       "
	dc.b " Testing VRAM resets all error counters."
	dc.b "                                        "	
	dc.b " The program pauses when it finds an    "
	dc.b " error. Press START to resume. Hold     "
	dc.b " START while testing to skip the pause. "
	dc.b "              Copyright 2019 ComradeOj. "
	dc.b "           Contact: ComradeOj@yahoo.com%"	
		
crash_screen:	
	dc.b "  The game has struck a fatal error.                            "	
	dc.b " Please E-mail a screenshot of this to:                         "
	dc.b "       > ComradeOj@Yahoo.com <                                  "
	dc.b " unless you caused the error on purpose.                        "			
	dc.b "  Also include a description of what                            "
	dc.b "  you were doing when the game crashed                          "
	dc.b "----------------------------------------------------------------"
	dc.b "  Error type:                                                   "
    dc.b "                                                                "
	dc.b "           *Data registers*                                     "	
	dc.b "                                                                "
	dc.b " D0:$???????? D1:$???????? D2:$????????                         "
	dc.b " D3:$???????? D4:$???????? D5:$????????                         "
	dc.b " D6:$???????? D7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Address registers*                                   "	
	dc.b "                                                                "
	dc.b " A0:$???????? A1:$???????? A2:$????????                         "
	dc.b " A3:$???????? A4:$???????? A5:$????????                         "
	dc.b " A6:$???????? A7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Special registers*                                   "
	dc.b "                                                                "
	dc.b "        Program counter:$??????                                 "
	dc.b "        Status register:$????                                   "
	dc.b "           Game version: 2.4                                    "
	dc.b "                                                                "
	dc.b "                                                                "	

palette_bleed:
	dc.w $0000,$0000,$000E,$00E0,$0E00,$0EEE,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
img_bleed:	
	incbin "graphics/testslides/bleed.kos"
		
palette_sharpness:
	dc.w $0666,$0666,$0000,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
img_sharpness:	
	incbin "graphics/testslides/sharpness.kos"
	
palette_overscan:
	dc.w $000E,$000E,$0000,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
img_overscan:	
	incbin "graphics/testslides/overscan.kos"
	
palette_stripes:
	dc.w $0EEE,$0EEE,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
img_stripes:	
	incbin "graphics/testslides/stripes.kos"
	
palette_checker:
	dc.w $0000,$0EEE,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
img_checker:	
	incbin "graphics/testslides/checker.kos"
	
palette_grad1:
	dc.w $0000,$0002,$0004,$0006,$0008,$000a,$000c,$000E
	dc.w $0000,$0200,$0400,$0600,$0800,$0a00,$0c00,$0E00
palette_grad2:
	dc.w $0000,$0020,$0040,$0060,$0080,$00a0,$00c0,$00E0
	dc.w $0000,$0222,$0444,$0666,$0888,$0aaa,$0ccc,$0Eee
palette_grad3:
	dc.w $0000,$0202,$0404,$0606,$0808,$0a0a,$0c0c,$0e0e
	dc.w $0000,$0022,$0044,$0066,$0088,$00aa,$00cc,$00ee
img_grad1:	
	incbin "graphics/testslides/grad1.kos"

	
Eurotext:
	dc.b "Europe         %"
NAtext:
	dc.b "North America  %"
Japantext:
	dc.b "Japan          %"
Japan50text:
	dc.b "Japan (50HZ)   %"	
yestext:
	dc.b "YES  %"
notext:
	dc.b "NO   %"	
nonetext:
	dc.b "None (normal)      %"
unknowntext:
	dc.b "Unknown        %"		
model3text:
	dc.b "Working (model 3)  %"
	
text_000:
	dc.b "68000%"
text_010:
	dc.b "68010%"	
noise:
	incbin "noise.bin"
	
sprites:
	incbin "graphics/sprite1.bin"
	incbin "graphics/sprite2.bin"
	incbin "graphics/tiles.bin"
	
tilemap:
	incbin "graphics/tilemap.bin"	
	

	incbin "graphics/image.bmp"	
dmaimg:
	
testmusic:
	incbin "sound/FMtest2.vgm"
	;incbin "sound/FMtest.vgm"
psg_track:
	incbin "sound/psg.vgm"
	;incbin "sound/quartet title.vgm"
DacDat:
	incbin "sound/sega.PCM"	
	
	