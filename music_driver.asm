music_driver:
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   		
vgm_loop:
        move.b (a2)+,d0
		cmpi.b #$61,d0
		 beq wait
		 cmpi.b #$66,d0
		 beq loop_playback
		cmpi.b #$52,d0 
		 beq update2612_0
		cmpi.b #$53,d0 
		 beq update2612_1
		cmpi.b #$50,d0
		 beq update_psg
		bra vgm_loop     
update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra vgm_loop
		
update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra vgm_loop
		
loop_playback:
        rts
		
kill_ym2612:		
        move.b #$28,$A04000
        move.b #$00,$A04001
        move.b #$01,$A04001
        move.b #$02,$A04001
        move.b #$03,$A04001
        move.b #$04,$A04001
        move.b #$05,$A04001
        move.b #$06,$A04001			
		rts	
kill_psg:
 		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011
		rts			
	
update_psg:
        move.b (a2)+,$C00011
		bra vgm_loop
	
wait:		
		clr d0
		clr d1
		move.b (a2)+,d1
		move.b (a2)+,d0			
		lsl.w #$08,d0    ;switch to big endian
		eor.w d0,d1      ;ditto
		;lsl #$01,d1     ;tempo adjust (removed in favor of more NOPs)
		bra waitloop
		
waitloop:
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		nop		
		nop		
		nop		
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		dbf d1,waitloop
		bra vgm_loop
		
		
      